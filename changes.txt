Angband 3.1.0
=============

This is an Angband development version -- it may be buggy, so be warned!

Thanks go to Eytan Zweig, pelpel, and Leon Marrick for patches used in
this version.

Changes up to now:

Bigger
------

* Add Ey-style quickstart.  (Eytan Zweig)

* Add trap detection indicator. (pelpel)  Also, make detection spells
  circular, of radius 22, and magic mapping circular of radius 30.

* Adopted various parts of the OAngband dungeon generation code.
  (Leon Marrick) For more information, please see:
  http://dev.rephial.org/trac/attachment/ticket/156/notes.txt

* Transient indicators now appear on the bottom line, starting at the
  left of the dungeon display; things like depth and speed appear on
  the main sidebar.  As a result of this, every timed effect is now
  displayed on the status bar.

Smaller
-------

* Monster recall colours changed somewhat, they're now closer to Ey,
  which I find much more readable.

Invisible (or almost so)
---------

